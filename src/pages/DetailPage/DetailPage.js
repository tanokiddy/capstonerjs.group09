import React from "react";
import BookingByMovie from "./BookingByMovie";
import DetailMovie from "./DetailMovie";

export default function DetailPage() {
  return (
    <div>
      <DetailMovie />
      <BookingByMovie />
    </div>
  );
}
