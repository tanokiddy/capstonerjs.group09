import React from "react";
import ContactHome from "./ContactHome";
import Copyright from "./Copyright";

export default function Footer() {
  return (
    <div>
      <ContactHome />
      <Copyright />
    </div>
  );
}
